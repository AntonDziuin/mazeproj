﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestL
{
    public partial class Form1 : Form
    {
        private static Random rnd;
        int size = 10;
        double diagonal;
        public Form1()
        {
            InitializeComponent();
            rnd = new Random();
            diagonal = Math.Sqrt(Math.Pow(size, 2) + Math.Pow(size, 2));
            StartBuildMaze();
        }
        void StartBuildMaze()
        {
            init();
            new Thread(buildMaze).Start();
        }

        int[,] field;
        int imageOffset = 2;
        void init()
        {
            field = new int[size, size];
            pictureBox1.Image = new Bitmap(size*10+ imageOffset*2, size*10 + imageOffset * 2);
            #region drawFrame
            for (int x = 0; x < size * 10 + imageOffset * 2; x++)
            {
                ((Bitmap)pictureBox1.Image).SetPixel(x, size * 10 + imageOffset * 2-1, Color.FromArgb(0, 0, 0));
                ((Bitmap)pictureBox1.Image).SetPixel(x, 0, Color.FromArgb(0, 0, 0));
            }
            for (int y = 0; y < size * 10 + imageOffset * 2; y++)
            {
                ((Bitmap)pictureBox1.Image).SetPixel(size * 10 + imageOffset * 2 - 1, y, Color.FromArgb(0, 0, 0));
                ((Bitmap)pictureBox1.Image).SetPixel(0, y, Color.FromArgb(0, 0, 0));
            }
            #endregion

        }

        void buildMaze()
        {
            position StartPosition = new position(rnd.Next(0, size), rnd.Next(0, size));
            position EndPosition;
            double dist;
            while (true)
            {
                EndPosition = new position(rnd.Next(0, size), rnd.Next(0, size));

                dist = Math.Sqrt(Math.Pow(EndPosition.x,2)+ Math.Pow(EndPosition.y, 2)- Math.Pow(StartPosition.x, 2) + Math.Pow(StartPosition.y, 2));
                if (dist> diagonal / 2)
                {
                    break;
                }
            }
            Console.WriteLine("Start position = {0},{1}", StartPosition.x, StartPosition.y);
            Console.WriteLine("End position = {0},{1}", EndPosition.x, EndPosition.y);
            Console.WriteLine("Dist = " + dist);

            field[StartPosition.x, StartPosition.y] = 1;
            position CurrentPos = new position(StartPosition.x, StartPosition.y);
            int rndNumber = -1;
            while (true)
            {
                List<position> positions = GetPosiblePositions(CurrentPos);
                if (positions.Count == 0)
                    break;
                rndNumber = rnd.Next(0, positions.Count);
                CurrentPos.x = positions[rndNumber].x;
                CurrentPos.y = positions[rndNumber].y;
                field[positions[rndNumber].x, positions[rndNumber].y] = 1;
                Invoke(new Action(() => {
                    for (int x= CurrentPos.x*10;x< CurrentPos.x * 10 + 10; x++)
                    {
                        if (x == CurrentPos.x * 10 || x ==  CurrentPos.x * 10 + 9) continue;
                        for (int y = CurrentPos.y * 10; y < CurrentPos.y * 10 + 10; y++)
                        {
                            if (y == CurrentPos.y * 10 || y == CurrentPos.y * 10 + 9) continue;
                            ((Bitmap)pictureBox1.Image).SetPixel(y+imageOffset, x + imageOffset, Color.FromArgb(0, 0, 0));
                        }
                    }
                    
                    pictureBox1.Refresh();
                    DrawAll(); }));
                    
                //   Thread.Sleep(40);
                //if (CurrentPos.Equals(EndPosition))
                //    break;
                
            }
        }
        List<position> GetPosiblePositions(position from)
        {
            List<position> PosiblePositions = new List<position>();
            //left
            if (from.x != 0)
            {
                if (field[from.x - 1, from.y] == 0)
                {
                    PosiblePositions.Add(new position(from.x - 1, from.y));
                }
            }
            //down
            if (from.y != 0)
            {
                if (field[from.x , from.y - 1] == 0)
                {
                    PosiblePositions.Add(new position(from.x , from.y - 1));
                }
            }
            //right
            if (from.x != size - 1)
            {
                if (field[from.x + 1, from.y] == 0)
                {
                    PosiblePositions.Add(new position(from.x + 1, from.y));
                }
            }
            //up
            if (from.y != size - 1)
            {
                if (field[from.x, from.y + 1] == 0)
                {
                    PosiblePositions.Add(new position(from.x, from.y + 1));
                }
            }
            return PosiblePositions;
        }
        void DrawAll()
        {
            label1.Text = "";
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    draw(field[x, y]);
                }
                //Console.WriteLine("");
                LabelOut(Environment.NewLine);
            }
        }
        void draw(int obj)
        {
            switch (obj)
            {
                case 0:
                    //Console.Write("   ");
                    LabelOut("   ");
                    break;
                case 1:
                    //Console.Write(" o ");
                    LabelOut(" o ");
                    break;
            }
        }
        void LabelOut(string s)
        {
            label1.Text += s;
        }
        struct position
        {
            public position(int x,int y)
            {
                this.x = x;
                this.y = y;
            }
            public int x { get; set; }
            public int y { get; set; }
            public override bool Equals(object obj)
            {
                if (!(obj is position))
                    return false;

                position pos = (position)obj;
                if (x == pos.x && y == pos.y)
                    return true;
                else
                    return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StartBuildMaze();
        }
    }
}
